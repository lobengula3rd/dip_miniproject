#ifndef _BINARIZER_NIBLACK_H_ 
#define _BINARIZER_NIBLACK_H_ 

#include "ManuscriptExport.h"
#include "cv_macros.h"
#include "imagebinarizer.h"


using namespace std ;

class MANUSCRIPT_EXPORT BinarizerNiblack : public ImageBinarizer{
	Mat _mean ;
	Mat _std  ;
	Mat _threshold ;
	Size   _window ;
	float  _k       ;
	double _max_std ;

public:
	BinarizerNiblack(void);
	BinarizerNiblack(int width, int height);
	~BinarizerNiblack(void);

	void   setWindow(Size w )  { _window = w ; }
	void   setK(float k)       { _k = k ; }
	double setMeanStdMaps(Mat img, Size window);
	void   setThreshold(Size size, float k);

	DImage* binarize();
};

#endif