#include "stdafx.h"
#include "DImage.h"
#include <iostream>
#include "Main.h"
#include "TextLine.h"
#include "TextLineExtractorGrid.h"
#include <limits>


int main(int argc, char** argv)
{

	if (argc<3){
		cout << "Please provide arguments: (1)[image file path] (2)[line height (pixels)]\n";
		return 0;
	}

	//Handle input
	string imagePath = argv[1];
	int lineHeight = atoi(argv[2]); //pixels
	Mat src = imread(imagePath, 1);

	TextLineExtractorGrid* textLineExtractorGrid = new TextLineExtractorGrid(src, lineHeight);
	//TextLineExtractorGrid* textLineExtractorGrid = new TextLineExtractorGrid(src, lineHeight, 0.9);


	vector<TextLine*> textLines;
	textLineExtractorGrid->extract(textLines);

	// 9. draw lines on source image and print information to screen
	cout << "\n\n\nResult Lines:\n";
	for (int i = 0; i < textLines.size(); i++){
		if (i % 2 == 0){
			textLines[i]->drawLine(Scalar(0,0,255));
		}
		else {
			textLines[i]->drawLine(Scalar(255,0,0));
		}
		textLines[i]->printLine();
	}

	// 10. print src image
	namedWindow("result", CV_WINDOW_AUTOSIZE);
	imshow("result", src);


	waitKey(0);
	return 0;
}